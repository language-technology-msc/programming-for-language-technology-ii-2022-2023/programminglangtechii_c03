# -*- coding: utf-8 -*-
"""
Created on Sun Apr  4 09:21:50 2021

@author: user
"""

import pandas as pd
import audio_file_representation as afr
import pickle

with open('data/data_representations.pickle', 'rb') as handle:
    loaded_structure = pickle.load(handle)

# %% isolate useful information
tmp_dict = {
    'actor_ID': [],
    'female': [],
    'emotion': [],
    'phrase_ID': [],
    'mean_centroid': [],
    'std_centroid': [],
    'mean_bandwidth': [],
    'std_bandwidth': [],
    'mean_contrast': [],
    'std_contrast': [],
    'mean_flatness': [],
    'std_flatness': [],
    'mean_rolloff': [],
    'std_rolloff': [],
    'mfcc_profile': [],
    'features': [],
}
# information is appended to the already loaded structure
# shallow copy
for r in loaded_structure:
    tmp_dict['actor_ID'].append( r.actor_ID )
    tmp_dict['female'].append( r.female )
    tmp_dict['emotion'].append( r.emotion )
    tmp_dict['phrase_ID'].append( r.phrase_ID )
    tmp_dict['mean_centroid'].append( r.mean_centroid )
    tmp_dict['std_centroid'].append( r.std_centroid )
    tmp_dict['mean_bandwidth'].append( r.mean_bandwidth )
    tmp_dict['std_bandwidth'].append( r.std_bandwidth )
    tmp_dict['mean_contrast'].append( r.mean_contrast )
    tmp_dict['std_contrast'].append( r.std_contrast )
    tmp_dict['mean_flatness'].append( r.mean_flatness )
    tmp_dict['std_flatness'].append( r.std_flatness )
    tmp_dict['mean_rolloff'].append( r.mean_rolloff )
    tmp_dict['std_rolloff'].append( r.std_rolloff )
    tmp_dict['mfcc_profile'].append( r.useful_mfcc_profile )
    tmp_dict['features'].append( r.features )

# %% create dataframe
df = pd.DataFrame( tmp_dict ).dropna()

# and save
df.to_pickle('data/prepared_dataframe.pickle')